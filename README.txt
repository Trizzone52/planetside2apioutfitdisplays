Enjin PlanetSide2 Member Specialization: Displays Character Cert Specializations for Character/Member Currently Logged in to Enjin.
Copyright (C) 2014  Tyrone Russell, trizzone52.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
Module to add to Planetside 2 Outfit website that tracks which members are logged in to Planetside and what classes they are Certed into for specialty squads.
Designed for Enjin, but should require only minor adjustments to take to any html website.


ps2OutfitOnlineSimple: Populates a div with members of a given outfit that are currently logged into PS2.

ps2OutfitOnlineWRank: Same as simple but also displays outfit rank. Primarily for example/resource purposes.

ps2OutfitOnlineWRankIcon: Visually viable version. Updated to sort by rank.

ps2OutfitOnlineSpecializationTable: Populates a sortable table with online outfit members currently logged into PS2 and their Cert specializations.  Requires sorttable.js.

ps2OutfitOnlineEnjinIndividual: Takes the display name of the currently logged in Enjin member, and pings SOE Census API to gain Cert specialization information about that character. Pulls jQuery from Google's hosting.
